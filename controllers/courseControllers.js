const mongoose = require("mongoose");
const Course = require("../models/course.js");


// Function for adding a course

/*module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	return newCourse.save().then((newCourse, error) =>
	{
		if (error){
			return error;		
		}
		else{
			return newCourse;
		}
	})

}*/

// Activity
// 2. Update the "addCourse"  controller method to implement admin authentication for creating a course
// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by a user that is not admin


module.exports.addCourse = (reqBody, result) => {
	if(result.isAdmin == true){
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return newCourse.save().then((newCourse, error) =>
	{
		if (error){
			return error;		
		}
		else{
			return newCourse;
		}
	})
}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});

	}
}


// Get all Courses
module.exports.getAllCourse = () => {
	return Course.find({}).then(result =>{
		return result;
	})
}


// Get all active Courses
module.exports.getActiveCourse = () => {
	return Course.find({isActive: true}).then(result =>{
		return result;
	})
}

// Get a specific Course
module.exports.getSpecificCourse = (courseId) => {
	return Course.findById(courseId).then(result =>{
		return result;
	})
}

// Updating a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
	

}
	

//2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
module.exports.courseArchive = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				isActive: newData.course.isActive
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}
