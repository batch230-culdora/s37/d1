const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Course = require("../models/course.js")

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
		// 10 - salt
	})

	return newUser.save().then((user,error) =>
	{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// function checkEmailExist{}
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// function for log-in{}
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); // true or false

			if(isPasswordCorrect){
				// Let's give the user a token to access features
				return{access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;
			}
		}
	})
}


// Activity - s38
/*
Activity Instructions
1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
2. Create a getProfile controller method for retrieving the details of the user:
a. Find the document in the database using the user's ID
b. Reassign the password of the returned document to an empty string
c. Return the result back to the frontend
3. Process a POST request at the /details route using postman to retrieve the details of the user
*/
	
/*module.exports.getProfile = (reqBody) => {
	 return User.find({_id: reqBody._id}).then(result =>{
		if(result != null){
			return result;
			}
		})
	}*/

// Answer on activity - s38

/*	module.exports.getProfile = (reqBody) => {
	 return User.findById({_id: reqBody._id}).then((result, error) =>{
		if(error){
			return false;
		}
		else{
			result.password = "";
			return result;
			}
		
		});
	};*/

/*module.exports.getProfile = (reqBody) => {
	 return User.findById(reqBody._id).then((result, error) =>{
		if (error){
			return false;
			}
		else{
			result.password = "";
			return result;
		}
	})
		
}*/


/*module.exports.getProfile = (reqBody) => {
	 return User.findById(reqBody._id).then((result, error) =>{
		if (error){
			return false;
			}
		else{
			result.password = "";
			return result;
		}
	})
		
}*/

// s41
module.exports.getProfile = (request, response) => {

	// will contain your decoded token
	const userData = auth.decode(request.headers.authorization);

	console.log(userData)

	return User.findById(userData.id).then(result => {
		result.password = "*****";
		response.send(result);
	})
}



// enroll feature
module.exports.enroll = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	let courseName = await Course.findById(request.body.courseId).then(result => result.name);

	let newData = {
		// User ID and email will be retrieved from the request header (request header contains the user token)
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrived from the request body
		courseId: request.body.courseId,
		courseName: courseName
	}

	console.log(newData);

	// a user document is updated if we received a "true" value
	let isUserUpdated = await User.findById(newData.userId)
	.then(users => {
		users.enrollments.push({
			courseId: newData.courseId,
			courseName: newData.courseName
		});
	
		// Save the updated user information from the database
		return users.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})

	console.log(isUserUpdated);

	let isCourseUpdated = await Course.findById(newData.courseId).then(courses =>{
		courses.enrollees.push({
			userId: newData.userId,
			email: newData.email
		})
	
		// Mini Activity
		// Create a condition that if the slots is already zero, no deduction of slots will happen and...
		// it should have a message in the terminal that the slot is already zero
		// Else if the slot is negative value, it should make the slot value to zero
		// Minus the slots available by 1
		// course.slots = course.slots -1;
		
		if(courses.slots == 0){
		console.log("No slots available");
		}
		else if(courses.slots < 0){
			courses.slots = 0;
		}
		else{
		courses.slots -= 1;
		}


			return courses.save()
		.then(result =>{
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		});
	})
	console.log(isCourseUpdated);

	// Condition will check if the both "user" and "course"
	// Ternary operator
	(isUserUpdated == true && isCourseUpdated == true)? response.send(true): response.send(false);
	/*
		if(isUserUpdated == true && isCourseUpdated == true){
			response.send(true);
		}
		else{
			response.send(false);
		}
	*/
}



	// Get all users
module.exports.getAllUser = () => {
	return User.find({}).then(result =>{
		return result;
	})
}


