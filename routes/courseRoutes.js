const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Creating a Course
/*router.post("/create", (request, response) => {
	courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController));
})*/


// Activity 
// 1. Refractor the "course" route to implement user authentication for the admin when creating a course
router.post("/create", auth.verify, (request, response) => 
{
		const result = {
			course: request.body, 	
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}
	courseControllers.addCourse(request.body, result).then(resultFromController => response.send(resultFromController));
})



// Get all Courses
router.get("/all", (request, response) => {
	courseControllers.getAllCourse(request.body).then(resultFromController => response.send(resultFromController));
})

// Get  all active Courses
router.get("/active", (request, response) => {
	courseControllers.getActiveCourse(request.body).then(resultFromController => response.send(resultFromController));
})

// Get a specific Courses
router.get("/:courseId", (request, response) => {
	courseControllers.getSpecificCourse(request.params.courseId).then(resultFromController => response.send(resultFromController));
})



// Get a specific Courses
router.patch("/:courseId/update", auth.verify, (request,response) => 
	{
		const newData = {
			course: request.body, 	//request.headers.authorization contains jwt
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
			response.send(resultFromController)
		})
	})




// additional activity
/*
1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
3. Process a PATCH(or PUT) request at the /courseId/archive route using postman to archive a course
4. Create a git repository named S35.
5. Add another remote link and push to git with the commit message of Add activity code - S35.
6. Add the link in Boodle.

*/
// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.courseArchive(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


module.exports = router;