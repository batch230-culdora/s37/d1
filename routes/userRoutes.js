const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/register", (request, response) => {
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController));

})

router.post("/checkEmail", (request, response) => {
	userControllers.checkEmailExists(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/login", (request, response) => {
	userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController));
})

/*router.post("/details", (request, response) => {
	userControllers.getProfile(request.body).then(resultFromController => response.send(resultFromController));
})*/

// Get all users
router.get("/allUsers", (request, response) => {
	userControllers.getAllUser(request.body).then(resultFromController => response.send(resultFromController));
})

// s41
router.get("/details", auth.verify, userControllers.getProfile);

router.post("/enroll", auth.verify, userControllers.enroll);


module.exports = router;

